#!/usr/bin/env python3
import parse
from neo4j.v1 import GraphDatabase, basic_auth
import getpass
import page_rank
import connected_components
def createIndices(session):
    session.run("CREATE INDEX ON :PAGE(key)")
    session.run("CREATE INDEX ON :TEXT(key)")
    session.run("CREATE INDEX ON :CHAPTER(key)")
    session.run("CREATE INDEX ON :SECTION(key)")
    session.run("CREATE INDEX ON :RECIPE(key)")
    session.run("CREATE INDEX ON :WORD(key)")

def addChapter(c, session):
    session.run("CREATE (:CHAPTER:PAGE {name: $name, key: $key, line: $line})-[:CONTENT]->(:TEXT {value:$text, key: $key})",
        name=c.name, key=c.key, text=c.text, line=c.line)

def addSection(s, session):
    session.run("CREATE (:SECTION:PAGE {name: $name, key:$key, line: $line})-[:CONTENT]->(:TEXT {value: $text, key: $key})", 
        name=s.name, key=s.key, text=s.text, line=s.line)
    session.run("""MATCH (p:PAGE {key:$parent}), (s:SECTION {key:$key})
            MERGE (p)-[:CONTAINS]->(s)""",
        parent=s.parent.key, key=s.key)

def addRecipe(r, session):
    session.run("CREATE (:RECIPE:PAGE {name: $name, key: $key, line: $line})",
        name=r.name, key=r.key, line=r.line)
    session.run("""MATCH (p:PAGE {key:$parent}), (r:RECIPE {key:$key})
            MERGE (p)-[:CONTAINS]->(r)""",
        parent=r.parent.key, key=r.key)

def addWord(w, session):
    session.run("CREATE (:WORD:PAGE {name: $name, words: $words, key: $key, line: $line})-[:CONTENT]->(:TEXT {value: $text, key: $key})",
        name=w.name, words=w.words, key=w.key, line=w.line, text=w.text)

def addRecipesText(items, session):
    recipes = [r for r in items if type(r) == parse.Recipe]
    withText = [r for r in recipes if r.hasText()] 
    withoutText = [r for r in recipes if not r.hasText()] 
    for r in withText:
        session.run("""MATCH (r:RECIPE {key: $key})
            MERGE (r)-[:CONTENT]->(:TEXT {value:$text, key: $key})""",
            text=r.text, key=r.key)
    for r in withoutText:
        session.run("""MATCH (r:RECIPE {key: $key}), (t:TEXT {key: $that})
            MERGE (r)-[:CONTENT]->(t)""",
            key=r.key, that=r.chaseTextRef().key)

def linkNextPages(items, session):
    for first, second in zip(items, items[1:]):
        session.run("""MATCH (f:PAGE {key: $first})
                    MATCH (s:PAGE {key: $second})
                    MERGE (f)-[:NEXT]->(s)""", 
                    first=first.key, second=second.key)

def addItem(i, session):
    if type(i) == parse.Recipe:
        addRecipe(i, session)
    elif type(i) == parse.Chapter:
        addChapter(i, session)
    elif type(i) == parse.Section:
        addSection(i, session)

def addCrossReferences(items, session):
    for i in items:
        for xref in i.crossReferences():
            session.run("MATCH (a:TEXT {key:$this}), (b:RECIPE {key:$that})\n"+
                "MERGE (a)-[:REFERENCES]->(b)",
                this=i.key, that=xref)

def linkGlossary(items, words, session):
    for i in items:
        for w in words:
            if w.inItem(i):
                session.run("""
                    MATCH (a:TEXT {key: $textKey}), (b:WORD {key:$wordKey})
                    MERGE (a)-[:USES]->(b)""",
                textKey=i.key, wordKey=w.key)

def addPageRank(items, session):
    pr = page_rank.page_rank(r for r in items if type(r) == parse.Recipe)
    for k, v in pr.items():
        session.run("MATCH (r:RECIPE {key:$key})\n"+
                "SET r.page_rank = $score",
                key=k, score=v["score"])

def addConnectedComponents(items, session):
    comp = connected_components.connected_components(
        r for r in items if type(r) == parse.Recipe
    )
    for component_id, group in enumerate(comp):
        for recipe_id in group:
            session.run("MATCH (r:RECIPE {key:$key})\n"+
                "SET r.component = $component",
                key=recipe_id, component=component_id)
if __name__ == "__main__":

    pw = getpass.getpass()
    driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", pw))

    # clean up database
    session = driver.session()
    transaction = session.begin_transaction()
    print("emptying")
    transaction.run("MATCH (n) DETACH DELETE n")
    transaction.commit()

    # create indexes
    transaction = session.begin_transaction()
    print("creating indexes")
    createIndices(transaction)
    transaction.commit()

    # add data
    transaction = session.begin_transaction()
    print("parsing")
    items = list(parse.recipes("trimmed.txt"))
    words = list(parse.glossary("glossary.txt"))

    print("adding %d items" % len(items))
    for i in items:
        addItem(i,transaction)

    print("adding %d definitions" % len(words))
    for w in words:
        addWord(w, transaction)

    print("adding text")
    addRecipesText(items, transaction)

    print("adding next page links")
    linkNextPages(items, transaction)

    print("adding cross references")
    addCrossReferences(items + words, transaction)

    print("linking glossary")
    linkGlossary(items, words, transaction)

    print("computing page rank")
    addPageRank(items, transaction)

    print("computing connected components")
    addConnectedComponents(items, transaction)

    print("commiting")
    transaction.commit()
