#!/usr/bin/env python3
import re
import json

contents = {}
with open("contents.json") as c:
    contents = json.load(c)

class Item():
    def __init__(self, name, line):
        self.name = name
        self.text = ""
        self.line = line
    def addText(self, text):
        self.text += text
    def crossReferences(self):
        xrefs = re.findall("No\. (\d+)", self.text) 
        return [int(x) for x in xrefs]
    def hasText(self):
        return self.text.strip() != ""
    def depth(self):
        if self.parent == None:
            return 0
        return self.parent.depth() + 1

class Chapter(Item):
    def __init__(self, chapter, line):
        self.key = chapter
        self.parent = None
        name = contents[chapter]
        super().__init__(name, line)

class Section(Item):
    def __init__(self, name, parent, line):
        self.parent = parent
        self.key = str(line) + name
        super().__init__(name, line)

class Recipe(Item):
    def __init__(self, parent, num, name, line):
        self.textRef = None
        self.parent = parent
        self.key = num
        super().__init__(name, line)

    def chaseTextRef(self):
        if self.textRef == None:
            return self
        else:
            return self.textRef.chaseTextRef()

class Word(Item):
    def __init__(self, words, definition, line):
        self.parent = None
        self.key = ", ".join(words)
        self.words = words
        super().__init__(self.key, line)
        self.text = definition

    def inItem(self,item):
        for w in self.words:
            if re.search(re.escape(w), item.text, re.IGNORECASE) != None:
                return True
        return False

def recipes(filename):
    numbers  = set()
    stack = []
    current = None
    with open(filename) as f:
        for lineno, line in enumerate(f):
            chapterHeading = re.match(r"CHAPTER [VIX]*", line)
            sectionHeading = re.match(r"==(.*)", line)
            recipeHeading = re.match(r"(\d+)— (.*)", line)
            if chapterHeading != None:
                name = line.strip()
                if current != None:
                    yield current
                current = Chapter(name, lineno)
                stack = [current]
            elif sectionHeading != None:
                name = sectionHeading.groups()[0].strip()
                if name != "":
                    if current != None:
                        yield current
                    current = Section(name, stack[-1], lineno)
                    stack.append(current)
                else:
                    stack.pop()
            elif recipeHeading != None:
                n = int(recipeHeading.groups()[0])
                if n in numbers:
                    print("duplicate %d" % n)
                numbers.add(n)
                new = Recipe(stack[-1],  n, recipeHeading.groups()[1], lineno)
                if current != None:
                    if type(current) == Recipe:
                        if not current.hasText():
                            current.textRef = new
                    yield current
                current = new
            else:
                if current != None:
                    current.addText(line)
    if current != None:
        yield current

def glossary(filename):
    with open(filename) as f:
        for lineno, line in enumerate(f):
            (namestring, definition) = line.split(",", 1)
            definition = definition.strip()
            names = [n.strip() for n in namestring.split("/")]
            yield Word(names, definition, lineno)

if __name__ == "__main__":
    words = list(glossary("glossary.txt"))

    for r in recipes("trimmed.txt"):
        print("  " * r.depth() + type(r).__name__ + " " + r.name)
        #print(r.text)
        xrefs = r.crossReferences()
        if len(xrefs) > 0:
            print(xrefs)
        for w in words:
            if w.inItem(r):
                print(w.words)


