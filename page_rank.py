#!/usr/bin/env python3

def page_rank(recipes, steps=10, damping=0.85):
    d = {k.key: {"score": 1.0, "links":k.crossReferences()} for k in recipes}
    for i in range(0, steps):
        new = { k:{"score":v["score"] * (1.0-damping), "links":v["links"]} for (k,v) in d.items() }

        for k, v in d.items():
            sz = len(v["links"]) 
            if sz != 0:
                for dest in v["links"]:
                    new[dest]["score"] += damping * v["score"] / sz
            else:
                new[k]["score"] += damping * v["score"]
        d = new

    return d

if __name__ == "__main__":
    import parse 
    
    ranked = sorted( 
        page_rank(r for r in parse.recipes("trimmed.txt") if type(r)==parse.Recipe).items(),
        key=(lambda x: 1.0-x[1]["score"])
    )

    for k, v in ranked[0:10]:  
        print("%f: %d" % (v["score"], k))
