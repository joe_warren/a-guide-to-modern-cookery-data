#!/usr/bin/env python3
from flask import Flask, render_template, send_file, send_from_directory, request, Markup, redirect, url_for
from jinja2 import evalcontextfilter, escape
from neo4j.v1 import GraphDatabase, basic_auth
import getpass
import re
import itertools
import string
import json

app = Flask(__name__)
PASSWORD = ""
NEO_URI = "bolt://localhost:7687"
NEO_USER= "neo4j"
DRIVER = None

def createSession():
    return DRIVER.session()
#@app.route("/static/")
#def static():
#    return send_from_directory('static', path)

class Item():
    def __init__(self, node):
        self.label = next(l for l in iter(node.labels) if l != "PAGE")
        self.dict = { k:v for k, v in node.items() }

    def __getitem__(self, key):
        return self.dict[key]

def Recipe(i):
    item = Item(i)
    if item.label != "RECIPE":
        raise "Invalid Item"
    d = item.dict
    d["formattedKey"] = "No." + formatReferenceNumber(d["key"])
    return d

class Relationship():
    def __init__(self, edge, out):
        self.out = out
        self.label = edge.type
        self.dict = { k:v for k, v in edge.items() }

    def __getitem__(self, key):
        return self.dict[key]

    def description(self):
        if self.label == "CONTAINS":
            if self.out:
                return "↳"
            else:
                return "↰"
        return self.label

_paragraph_re = re.compile(r'\n\n')

@app.template_filter()
@evalcontextfilter
def par2br(eval_ctx, value):
    result = u'\n\n'.join(u'<p>%s</p>' % p \
        for p in _paragraph_re.split(escape(value)))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result

@app.template_filter()
@evalcontextfilter
def listify(eval_ctx, value):
    def islistitem(s):
        return (len(s) != 0 and s[0] == "*")
    def aslist(lines):
        inlist = False
        for s in lines:
            if inlist:
                if islistitem(s):
                    yield "<li>%s</li>" % s[1:]
                else:
                    inlist=False
                    yield "</ul>%s" % s
            else:
                if islistitem(s):
                    inlist=True
                    yield "<ul><li>%s</li>" % s[1:]
                else:
                    yield  s
        if inlist:
            yield "</ul>"
    
    line_re = re.compile(r'\n')

    result = u'\n'.join( aslist( line_re.split(escape(value))))

    if eval_ctx.autoescape:
        result = Markup(result)
    return result

def formatReferenceNumber(num):
    if num < 10000:
        return str(num)
    else:
        numpart = int(num)%10000
        subpart = int(num/10000)
        letter = string.ascii_lowercase[subpart - 1]
        return str(numpart) + letter

@app.template_filter()
@evalcontextfilter
def referencenumber(eval_ctx, value):
    return formatReferenceNumber(value)

@app.template_filter()
@evalcontextfilter
def referencelinks(eval_ctx, value):
    values = zip( 
        re.split( r"No\. (\d+)", escape(value)), 
        itertools.cycle([False, True])
    )
    def parseValue(text, isRef):
        if not isRef:
            return text
        else:
            i = int(text)
            return "<span class='RECIPE'><a href='%s'>No. %s</a></span>" % \
              (url_for("item", label="RECIPE", key=i), formatReferenceNumber(i))
            
    result = "".join(parseValue(text, isRef) for text, isRef in values)
    if eval_ctx.autoescape:
        result = Markup(result)
    return result


@app.template_filter()
@evalcontextfilter
def insertInlineGlossary(eval_ctx, value, glossary):
    value = escape(value)
    for item, w in glossary:
        for n in item["words"]:
            values = list(zip( 
                re.split( "(%s)" % re.escape(n), value, flags=re.IGNORECASE), 
                itertools.cycle([False, True])
            ))
            print(values)
            def parseValue(text, isRef):
                if not isRef:
                    return text
                else:
                    return "<span title='%s'>%s</span>" % \
                        (w, text)

            value = "".join(parseValue(text, isRef) for text, isRef in values)
    if eval_ctx.autoescape:
        value = Markup(value)
    return value

@app.template_filter()
@evalcontextfilter
def highlight(eval_ctx, value, query, context):

    tokens = re.split("(%s)"%re.escape(query), value, flags=re.IGNORECASE)

    if len(tokens) == 1:
        # the field wasn't found, return the first <context> characters
        result = value[0:context]
        if len(value) > context:
            result += ". . ."
        if eval_ctx.autoescape:
            result = Markup(result)
        return result

    parts = tokens[::2]
    separators = tokens[1::2]

    def truncateFirst(f):
        if len(f) > context:
            return ". . ." + f[-context:]
        return f

    def truncateMiddle(m):
        if len(m) > context * 2:
            return m[:context] + ". . .<br/>. . ." + m[-context:]
        return m

    def truncateLast(l):
        if len(l) > context:
            return l[:context] + ". . ."
        return l

    first = truncateFirst(parts[0])
    middle = [truncateMiddle(m) for m in parts[1:-1]]
    last = truncateLast(parts[-1])

    parts = [first] + middle + [last]
    separators = ["<span class='hl'>%s</span>" % s for s in separators]

    tokens = parts + separators
    tokens[0::2] = parts
    tokens[1::2] = separators
    result = "".join(tokens)

    if eval_ctx.autoescape:
        result = Markup(result)
    return result

@app.route("/contents")
def contents():
    session = createSession()
    res = session.run("""
        MATCH (b:CHAPTER{})
        WITH collect({ len:0, a: b }) as chapters
        MATCH p1 = (b:CHAPTER{})-[:CONTAINS*0..10]->(a1:SECTION) 
        WITH chapters + collect({ len:LENGTH(p1), a: a1 }) as all
        UNWIND all AS row
        RETURN row.len as len, row.a as a
        ORDER BY a.line""")
    return render_template("contents.html", 
        contents = ((r["len"], Item(r["a"])) for r in res)
    )

@app.route("/item/<label>/<key>")
def item(label, key):
    if label == "WORD":
        return redirect(url_for('glossary',_anchor=key))
    session = createSession()

    if label == "RECIPE":
        key = int(key)
    
    this = Item(next(iter(session.run("""MATCH (n:PAGE {key: $key})
            RETURN n""",
        label=label, key=key)))["n"])

    componentCount = 0
    if label == "RECIPE":
        componentCount = next(iter(session.run("""
            MATCH (:RECIPE {component: $component_id})
            RETURN COUNT(*) as count""",
            component_id = this["component"])))["count"]

    contents = list(session.run("""
        MATCH p1 = (:PAGE {key:$key})-[:CONTAINS*1..10]->(a1:PAGE) 
        WITH collect({ len:LENGTH(p1), a: a1 }) as first
        MATCH p2 = (:PAGE {key:$key})<-[:CONTAINS*0..10]-(a2:PAGE) 
        WITH first + collect({ len:-LENGTH(p2), a: a2 }) as all
        UNWIND all AS row
        WITH row.len as l, row.a as n
        RETURN l, n
        ORDER BY n.line""",
        key=key))

    text = session.run("""MATCH (n {key: $key}) -[:CONTENT]-> (t:TEXT)
            WHERE $label IN labels(n)
            RETURN t""", 
        label=label, key=key)

    previousPage = session.run("""MATCH (n:PAGE {key: $key}) <-[:NEXT]- (p:PAGE)
            WHERE $label IN labels(n)
            RETURN p""", 
        label=label, key=key)

    nextPage = session.run("""MATCH (n:PAGE {key: $key}) -[:NEXT]-> (p:PAGE)
            WHERE $label IN labels(n)
            RETURN p""", 
        label=label, key=key)

    references = session.run("""MATCH (n {key: $key}) -[:CONTENT]-> (:TEXT)-[:REFERENCES]->(r)
            WHERE $label IN labels(n)
            RETURN r
            ORDER BY r.line""", 
        label=label, key=key)

    referencedBy = session.run("""MATCH (n {key: $key}) <-[:REFERENCES]-> (:TEXT)<-[:CONTENT]-(r)
            WHERE $label IN labels(n)
            RETURN r
            ORDER BY r.line""", 
        label=label, key=key)

    glossary = session.run("""MATCH (n {key: $key}) -[:CONTENT]-> (:TEXT)-[:USES]->(w:WORD)-[:CONTENT]->(t:TEXT)
            WHERE $label IN labels(n)
            RETURN w, t.value as t
            ORDER BY w.key""", 
        label=label, key=key)

    return render_template("item.html", 
        this = this,
        componentCount = componentCount,
        minContents = min(n["l"] for n in contents),
        contents=((n["l"], Item(n["n"])) for n in contents), 
        references = (Item(r["r"]) for r in references),
        referencedBy = (Item(r["r"]) for r in referencedBy),
        glossary = list((Item(r["w"]), r["t"]) for r in glossary),
        previousPage = (Item(p["p"]) for p in previousPage),
        nextPage = (Item(p["p"]) for p in nextPage),
        text="\n\n".join(t["t"]["value"] for t in text)
    )

@app.route("/components")
def components():
    with createSession() as session:
        components = session.run(""" 
    MATCH (n:RECIPE)
        WITH n
        ORDER BY n.page_rank DESC 
        WITH n.component as prop, COLLECT(n) as nodelist, COUNT(*) as count
        WHERE count > 2
        RETURN prop, nodelist[0..3] as top, count ORDER BY count DESC;
        """)
    return render_template("components.html", 
        components=({
            "component":c["prop"], 
            "count": c["count"], 
            "items": (Item(t) for t in c["top"])
        }  for c in components )
    )

@app.route("/component/<component_id>")
def component(component_id):

    with createSession() as session:
        edges = session.run(""" 
            MATCH (a:RECIPE {component:$component})-[:CONTENT]->(t:TEXT)-[:REFERENCES]->(b:RECIPE)
                RETURN a.key as A, b.key as B""", 
                component=int(component_id))

        nodes = list(session.run(""" 
            MATCH (a:RECIPE {component:$component})
                RETURN a""", 
                component=int(component_id)))
        js = {
            "nodes": [Recipe(a["a"]) for a in nodes],
            "edges":[{"source": i["A"], "target": i["B"]} for i in edges]
        }
        return render_template("component_graph.html", 
            graph = json.dumps(js), 
            nodes = (Item(a["a"]) for a in nodes)
        )

@app.route("/glossary")
def glossary():
    with createSession() as session:
        results = session.run(""" 
        MATCH (a:WORD)-[:CONTENT]->(t:TEXT)
            RETURN a, t.value as t ORDER BY a.key"""
            )
        return render_template("glossary.html", 
            words = ((Item(r["a"]), r["t"]) for r in results)
        )

@app.route("/search")
def search():
    q = request.args.get('q')
    print(q)
    with createSession() as session:
        results = session.run(""" 
        MATCH (a:PAGE)-[:CONTENT]->(t:TEXT)
        WHERE toLower(t.value) CONTAINS toLower($q) 
            OR toLower(a.name) CONTAINS toLower($q)
            RETURN a, t.value as t ORDER BY a.line""", 
            q=q)
        return render_template("search.html", 
            q=q,
            results = ((Item(r["a"]), r["t"]) for r in results)
        )
@app.route("/")
def index():
    return redirect(url_for("contents"))

if __name__ == "__main__":
    PASSWORD = getpass.getpass()
    DRIVER = GraphDatabase.driver(NEO_URI, auth=basic_auth(NEO_USER, PASSWORD))
    app.run()
