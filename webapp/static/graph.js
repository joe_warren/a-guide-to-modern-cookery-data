function getWidth() {
  return Math.max(
    document.body.scrollWidth,
    document.documentElement.scrollWidth,
    document.body.offsetWidth,
    document.documentElement.offsetWidth,
    document.documentElement.clientWidth
  );
}

function getHeight() {
  return Math.max(
    document.body.scrollHeight,
    document.documentElement.scrollHeight,
    document.body.offsetHeight,
    document.documentElement.offsetHeight,
    document.documentElement.clientHeight
  );
}

window.onload = function(){
    var svg = d3.select("svg"),
    width = getWidth(),
    height = getHeight();
    svg.attr("width", width).attr("height", height);

    svg.append("rect")
        .attr("width", width)
        .attr("height", height)
        .style("fill", "none")
        .style("pointer-events", "all")
        .call(d3.zoom()
            .scaleExtent([1 / 2, 4])
            .on("zoom", zoomed));

    var g = svg.append("g")

    function zoomed() {
        g.attr("transform", d3.event.transform);
    }

    var maxPr = d3.max(graph.nodes.map(x=>x.page_rank))

    var color = d3.scaleLinear().domain([1,maxPr])
      .interpolate(d3.interpolateHcl)
      .range([d3.rgb("#74AE97"), d3.rgb('#003A23')]);

    var simulation = d3.forceSimulation()
        .force("link", d3.forceLink().id(function(d) { return d.key; }))
        .force("charge", d3.forceManyBody())
        .force("x", d3.forceX(width / 2))
        .force("y", d3.forceY(height / 2));

   var link = g.append("g")
      .attr("class", "links")
    .selectAll("line")
    .data(graph.edges)
    .enter().append("line")
    .attr('marker-end','url(#arrowhead)');
    
  var node = g.append("g")
      .attr("class", "nodes")
    .selectAll("circle")
    .data(graph.nodes)
    .enter().append("g")
      .call(d3.drag()
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended))
      .on("click", function(d){
        d3.selectAll(".hidden").style("display", null);
        d3.select("._" + d.key).style("display", "inline");
      });

    node.append("circle")
      .attr("r", 15)
      .attr("fill", function(d) { return color(d.page_rank); });

    node.append("text").text(d=> d.formattedKey)
        .attr("fill", d => ((d.page_rank/maxPr > 0.5) ? "white" : "black"))

  node.append("title")
      .text(function(d) { return d.name; });

  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);

  simulation.force("link")
      .links(graph.edges)
      .distance(80);

    simulation.force("charge").strength(-120)

  simulation.force("x").strength(0.02);
  simulation.force("y").strength(0.02);

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("transform", function(d){
          return "translate(" + d.x + ", " + d.y + ")";
     })
  }

function dragstarted(d) {
    if (!d3.event.active) simulation.alphaTarget(0.3).restart();
    d.fx = d.x;
    d.fy = d.y;
}

function dragged(d) {
    d.fx = d3.event.x;
    d.fy = d3.event.y;
}

function dragended(d) {
    if (!d3.event.active) simulation.alphaTarget(0);
    d.fx = null;
    d.fy = null;
}

}
