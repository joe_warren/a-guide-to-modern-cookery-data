#!/usr/bin/env python3

def connected_components(recipes):
    links = {v.key: set(v.crossReferences()) for v in recipes}
    # make links bidirectional
    for k, v in links.items():
        for xref in v:
            links[xref].add(k)
    nodes = {v for v in links.keys()}
    result = []
    while nodes:
        n = nodes.pop()
        group = {n}
        queue = [n]
        while queue:
            n = queue.pop(0)
            neighbors = links[n]
            neighbors.difference_update(group)
            nodes.difference_update(neighbors)
            group.update(neighbors)
            queue.extend(neighbors)
        result.append(group)
    return result
    
if __name__ == "__main__":
    import parse 
    components = connected_components(
        r for r in parse.recipes("trimmed.txt") if type(r)==parse.Recipe
    )
    print(sorted(len(c) for c in components)[-10:])
